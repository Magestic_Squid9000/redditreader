package android.ramirezh.redditreader;

import android.net.Uri;

public interface ActivityCallBack {
    void onPostSelected(Uri redditPostUri);
}
