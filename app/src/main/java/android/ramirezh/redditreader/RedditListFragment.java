package android.ramirezh.redditreader;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class RedditListFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallBack activityCallBack;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallBack = (ActivityCallBack)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallBack = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reddit_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        new RedditPostTask().execute(this);

        return view;
    }

    public void updateUserINterface(){
        RedditPostAdapter adapter = new RedditPostAdapter(activityCallBack);
        recyclerView.setAdapter(adapter);
    }
}
