package android.ramirezh.redditreader;


import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class RedditPostAdapter extends RecyclerView.Adapter<RedditPostHolder>{

    private ArrayList<RedditPost> redditPosts;
    private ActivityCallBack activityCallBack;

    public RedditPostAdapter(ActivityCallBack activityCallBack){
        redditPosts = RedditPostParser.getInstance().redditPosts;
        this.activityCallBack = activityCallBack;
    }

    @Override
    public RedditPostHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
        LayoutInflater  inflater= LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new RedditPostHolder(view);
    }

    @Override
    public void onBindViewHolder(RedditPostHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri redditUri = Uri.parse(redditPosts.get(position).url);
                activityCallBack.onPostSelected(redditUri);
            }
        });


        holder.titleText.setText(redditPosts.get(position).title);
    }

    @Override
    public int getItemCount() {
        return redditPosts.size();
    }
}
